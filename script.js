'use strict';

window.addEventListener('DOMContentLoaded', () => {

    const rowsInput = document.querySelector('#row-input');
    const colsInput = document.querySelector('#col-input');
    const sbmtBtm = document.querySelector('#submit-btn');
    const tableContainer = document.querySelector('#table-container');
    const form = document.querySelector('#form');

    const createTable = () => {
        let rows = rowsInput.value;
        let cols = colsInput.value;

        const table = document.createElement('table');

        for (let i = 0; i < cols; i++) {
            const col = document.createElement('col');
            col.setAttribute('width', '100');
            table.appendChild(col);
        }

        for (let i = 0; i < rows; i++) {
            const row = document.createElement('tr');
            for (let i = 0; i < cols; i++) {
                const cell = document.createElement('td');
                row.appendChild(cell);
            }
            table.appendChild(row);
        }

        tableContainer.appendChild(table);
    };

    const clearTable = () => {
        tableContainer.innerHTML = '';
    };

    sbmtBtm.addEventListener('click', event => {
        event.preventDefault();
        clearTable();
        createTable();
        form.reset();
    });
});